+++
title = "Personal Info"
description = "Kejia personal information."
date = 2021-05-01T08:00:00+00:00
updated = 2021-05-01T08:00:00+00:00
draft = false
weight = 10
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = ''
toc = true
top = false
+++

<img src="../../getting-started/IMG_9201.JPG" width="115" height="136">

### Education
I got my BS in Computer Science degree from <a href="https://www.ubc.ca/">UBC</a>, a university located in <a href="https://en.wikipedia.org/wiki/Vancouver">Vancouver BC</a>, Canada.
Now, I am a <a href="https://cs.duke.edu/graduate/ms">MSCS</a> student at <a href="https://duke.edu/">Duke University</a>, which is located in <a href="https://en.wikipedia.org/wiki/Durham,_North_Carolina">Durham NC</a>, USA.

- **University of British Columbia**  
  B.Sc. Major in Computer Science (09/2019 - 05/2023)

- **Duke University**  
  M.Sc. Major in Computer Science (09/2023 - present)


### Skills

- **Programming**

  TypeScript, Java, Python, Swift, SQL, C++, C, PHP, R


- **Tools/Environment**

  VS Code, IntelliJ, Pycharm, Google Colab, Jupyter notebook,

  RStudio, DrRacket, Git, Postman


## My Projects

Find out my projects. [Project List →](../../contributing/how-to-contribute/)