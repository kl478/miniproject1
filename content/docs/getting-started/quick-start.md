+++
title = "Albums"
description = "One page summary of how to start a new AdiDoks project."
date = 2021-05-01T08:20:00+00:00
updated = 2021-05-01T08:20:00+00:00
draft = false
weight = 20
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = "My memories"
toc = true
top = false
+++

## Vancouver Downtown

<img src="../../getting-started/IMG_7247.png" width="450" height="602">

## Great Smoky Mountains

<img src="../../getting-started/IMG_4635.png" width="600" height="449">

## Duke Chapel

<img src="../../getting-started/IMG_1111.png" width="600" height="494">

## Duke East Campus

<img src="../../getting-started/IMG_1234.png" width="600" height="401">